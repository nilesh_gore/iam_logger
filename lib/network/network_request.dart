import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:iamlogger/constants/constanats.dart';
import 'package:iamlogger/model/device_status.dart';
import 'package:iamlogger/model/log_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkRequestProvider {
  static Future<List<LogData>> getLogsFromServer(int preIndex) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String url = (prefs.getString(Constants.baseURL) ?? "");
    List<LogData> result = <LogData>[];
    if (url != "") {
      try {
        final apiResponse = await http.get(
          Uri.parse("http://$url/getlogs?num=$preIndex"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': "accept:application/json",
            'Accept-Encoding': 'gzip, deflate, br',
          },
        );
        print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body)['messages'] is List) {
            int lastIndex = json.decode(apiResponse.body)['last_index'];
            for (int i = 0;
                i <= json.decode(apiResponse.body)['messages'].length;
                i++) {
              if (i == json.decode(apiResponse.body)['messages'].length) {
                result.add(LogData(
                    "-- Updated At ${DateTime.now().toLocal()} IST --",
                    lastIndex));
              } else {
                result.add(LogData(
                    json.decode(apiResponse.body)['messages'][i], lastIndex));
              }
            }
          }
        }
      } catch (e) {
        print(e);
      }
    }
    return result;
  }

  static Future<List<DeviceStatus>> getDeviceStatusFromServer() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String url = (prefs.getString(Constants.baseURL) ?? "");
    List<DeviceStatus> result = <DeviceStatus>[];
    if (url != "") {
      try {
        final apiResponse = await http.get(
          Uri.parse("http://$url/status_codes"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': "accept:application/json",
            'Accept-Encoding': 'gzip, deflate, br',
          },
        );
        // print(apiResponse.body);
        if (apiResponse.statusCode == 200) {
          if (json.decode(apiResponse.body)['devices'] is Map) {
            Map jsonStatusData = json.decode(apiResponse.body)['devices'];
            jsonStatusData.forEach((key, value) {
              // print('{ key: $key, value: $value }');
              result.add(DeviceStatus(
                  key,
                  value['last_rcv'].toString().replaceAll("T", " "),
                  value['current']));
            });
          }
        }
      } catch (e) {
        print(e);
      }
    }
    return result;
  }
}
