import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:iamlogger/model/device_status.dart';
import 'package:iamlogger/network/network_request.dart';
import 'package:toast/toast.dart';

class MyDeviceStatusPage extends StatefulWidget {
  MyDeviceStatusPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    return MyDeviceStatusPageState();
  }
}

class MyDeviceStatusPageState extends State<MyDeviceStatusPage> {
  int lastIndex = 0;
  List<DeviceStatus> rows = <DeviceStatus>[];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          iconSize: 30,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            onPressed: () {
              fetchData();
            },
            icon: Icon(Icons.wifi_protected_setup),
            tooltip: "Update Logcat",
          ),
          IconButton(
            onPressed: () {
              rows = <DeviceStatus>[];
              // lastIndex = 0;
              showToastMessage("Logcat Cleared", context);
              setState(() {});
            },
            icon: Icon(Icons.cleaning_services),
            tooltip: "Clear Logcat",
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
        child: HtmlWidget(
          getHtmlData(),
          factoryBuilder: () => MyWidgetFactory(),
        ),
      ),
    );
  }

  void showToastMessage(String msg, BuildContext context) {
    Toast.show(msg, context, gravity: Toast.bottom);
  }

  String getHtmlData() {
    var buffer = new StringBuffer();
    buffer
        .write('''<div  width="100%" height="100%"  style="color:black;" >''');
    buffer.write(
        '''<p style="color: white; font-weight: bold; font-size:2.5vw;font-size: 16px; text-align: center;">-------------------------------------------------------------</p>''');
    for (int i = 0; i <= rows.length; i++) {
      if (i == 0) {
        buffer.write(
            '''<p style="color: white; font-weight: bold; font-size:2.5vw;font-size: 16px; text-align: center;">&nbsp; &nbsp;Id &nbsp; &nbsp;| &nbsp; &nbsp;Device &nbsp; &nbsp;| &nbsp; &nbsp;Current &nbsp; &nbsp;| &nbsp; &nbsp;Last Receive At &nbsp; &nbsp;</p>''');
        buffer.write(
            '''<p style="color: white; font-weight: bold;font-size:2.5vw; font-size: 16px; text-align: center;">-------------------------------------------------------------</p>''');
      } else {
        if (int.parse(rows[i - 1].current) < 2000) {
          buffer.write(
              '''<p style="color: lightgreen; font-weight: bold; font-size:2.5vw;font-size: 16px; text-align: center;">&nbsp; &nbsp;$i&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].name}&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].current}&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].lastReceiveDate}&nbsp; &nbsp;</p>''');
        } else if (int.parse(rows[i - 1].current) >= 2000) {
          buffer.write(
              '''<p style="color: red; font-weight: bold;font-size:2.5vw; font-size: 16px; text-align: center;">&nbsp; &nbsp;$i&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].name}&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].current}&nbsp; &nbsp;|&nbsp; &nbsp;${rows[i - 1].lastReceiveDate}&nbsp; &nbsp;</p>''');
        }
      }
    }
    buffer.write(
        '''<p style="color: white; font-weight: bold;font-size:2.5vw; font-size: 16px; text-align: center;">-------------------------------------------------------------</p>''');
    buffer.write(
        '''<p style="color: gray; font-weight: bold;font-size:2.5vw; font-size: 16px; text-align: center;"> -- Updated At ${DateTime.now().toLocal()} IST --</p>''');
    buffer.write(
        '''<p style="color: white; font-weight: bold;font-size:2.5vw; font-size: 16px; text-align: center;">-------------------------------------------------------------</p>''');

    buffer.write('''</div>''');
    print(buffer.toString());
    return buffer.toString();
  }

  fetchData() async {
    List<DeviceStatus> result =
        await NetworkRequestProvider.getDeviceStatusFromServer();
    if (rows.length != result.length) {
      rows = result;
    }
    setState(() {});
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;

  String? get webViewUserAgent => 'iAM Logger';

  bool get webViewDebuggingEnabled => false;
}
