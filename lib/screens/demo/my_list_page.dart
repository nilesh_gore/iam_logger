import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:iamlogger/model/log_data.dart';
import 'package:iamlogger/model/user.dart';
import 'package:iamlogger/network/network_request.dart';
import 'package:pagination_view/pagination_view.dart';

class MyListPage extends StatefulWidget {
  MyListPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyListPageState createState() => _MyListPageState();
}

class _MyListPageState extends State<MyListPage> {
  int lastIndex = 0;
  int page = -1;
  PaginationViewType paginationViewType = PaginationViewType.listView;
  GlobalKey<PaginationViewState> key = GlobalKey();

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PaginationView<LogData>(
        key: key,
        paginationViewType: paginationViewType,
        itemBuilder: (BuildContext context, LogData logData, int index) =>
            ListTile(
          title: Text(logData.data),
          leading: Icon(
            Icons.error,
            color: Colors.red[700],
          ),
        ),
        pageFetch: pageFetch,
        pullToRefresh: true,
        onError: (dynamic error) => Center(
          child: Text('Some error occurred'),
        ),
        onEmpty: Center(
          child: Text('Sorry! This is empty'),
        ),
        bottomLoader: Center(
          child: CircularProgressIndicator(),
        ),
        initialLoader: Center(
          child: CircularProgressIndicator(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () => key.currentState?.refresh(),
        tooltip: 'REST',
        child: Icon(Icons.wifi_protected_setup),
      ),
    );
  }

  Future<List<LogData>> pageFetch(int offset) async {
    List<LogData> result =
        await NetworkRequestProvider.getLogsFromServer(lastIndex);
    page = (offset / result.length).round();
    return result;
  }

  Future<List<User>> pageFetchOld(int offset) async {
    page = (offset / 20).round();
    final Faker faker = Faker();
    final List<User> nextUsersList = List.generate(
      20,
      (int index) => User(
        faker.person.name() + ' - $page$index',
        faker.internet.email(),
      ),
    );
    await Future.delayed(Duration(seconds: 1));
    return page == 5 ? [] : nextUsersList;
  }

  fetchData() async {
    List<LogData> result =
        await NetworkRequestProvider.getLogsFromServer(lastIndex);
    if (result.length > 0) {
      lastIndex = result[0].lastIndex;
    }
  }
}
