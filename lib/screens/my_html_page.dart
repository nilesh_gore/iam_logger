import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:fwfh_webview/fwfh_webview.dart';
import 'package:iamlogger/constants/constanats.dart';
import 'package:iamlogger/model/log_data.dart';
import 'package:iamlogger/network/network_request.dart';
import 'package:iamlogger/screens/my_device_status_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class MyHtmlPage extends StatefulWidget {
  MyHtmlPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    return MyHtmlPageState();
  }
}

class MyHtmlPageState extends State<MyHtmlPage> {
  int lastIndex = 0;
  List<LogData> rows = <LogData>[];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            IconButton(
              onPressed: () {
                fetchData();
              },
              icon: Icon(Icons.wifi_protected_setup),
              tooltip: "Update Logcat",
            ),
            IconButton(
              onPressed: () {
                rows = <LogData>[];
                // lastIndex = 0;
                showToastMessage("Logcat Cleared", context);
                setState(() {});
              },
              icon: Icon(Icons.cleaning_services),
              tooltip: "Clear Logcat",
            ),
            IconButton(
              onPressed: () {
                displayIpAddressDialog(context);
              },
              icon: Icon(Icons.settings),
              tooltip: "Settings",
            ),
          ],
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: HtmlWidget(
            getHtmlData(),
            factoryBuilder: () => MyWidgetFactory(),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                  child: Row(
                children: [
                  Flexible(
                    child: Image.asset(
                      'assets/icon/iam_icon.PNG',
                      fit: BoxFit.scaleDown,
                    ),
                    flex: 2,
                  ),
                  Flexible(
                    child: Center(
                      child: Text(
                        'iAM Logger',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    flex: 4,
                  )
                ],
              )),
              ListTile(
                title: const Text('Device Status'),
                onTap: () {
                  // Update the state of the app
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return MyDeviceStatusPage(title: "Device Status");
                  }));
                  // Then close the drawer
                  // Navigator.pop(context);
                },
              ),
            ],
          ),
        ));
  }

  void showToastMessage(String msg, BuildContext context) {
    Toast.show(msg, context, gravity: Toast.bottom);
  }

  String getHtmlData() {
    var buffer = new StringBuffer();
    buffer
        .write('''<div  width="100%" height="100%"  style="color:black;" >''');
    for (int i = 0; i < rows.length; i++) {
      if (rows[i].data.contains("INFO")) {
        buffer.write(
            '''<p style="color:white;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      } else if (rows[i].data.contains("DEBUG")) {
        buffer.write(
            '''<p style="color:lightgreen;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      } else if (rows[i].data.contains("ERROR")) {
        buffer.write(
            '''<p style="color:red;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      } else if (rows[i].data.contains("WARNING")) {
        buffer.write(
            '''<p style="color:yellow;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      } else if (rows[i].data.contains("CRITICAL")) {
        buffer.write(
            '''<p style="color:red;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      } else if (rows[i].data.contains("Updated At")) {
        buffer.write(
            '''<p style="color:gray;font-weight:bold;font-size:2.5vw;font-size:16px;">${rows[i].data.replaceAll("\n", "")}</p>''');
      }
    }
    buffer.write('''</div>''');
    print(buffer.toString());
    return buffer.toString();
  }

  Future<void> displayIpAddressDialog(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var ipAddressTxt = (prefs.getString(Constants.baseURL) ?? "");
    TextEditingController ipAddress = TextEditingController(text: ipAddressTxt);
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('IP Address'),
            content: TextField(
              controller: ipAddress,
              autocorrect: true,
              showCursor: true,
              decoration: InputDecoration(hintText: "Eg 192.168.xx.xx:8x8x"),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Cancel',
                  style: TextStyle(),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                child: Text(
                  'Save',
                  style: TextStyle(),
                ),
                onPressed: () async {
                  rows = <LogData>[];
                  lastIndex = 0;
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.setString(Constants.baseURL, ipAddress.text.toString());
                  showToastMessage("IP address Updated..!!", context);
                  Navigator.pop(context);
                  fetchData();
                },
              ),
            ],
          );
        });
  }

  fetchData() async {
    // Dialogs.showLoadingDialog(context);
    List<LogData> result =
        await NetworkRequestProvider.getLogsFromServer(lastIndex);
    if (result.length > 0) {
      lastIndex = result[0].lastIndex;
    }
    rows = [result.reversed.toList(), rows].expand((x) => x).toList();

    // Navigator.of(context, rootNavigator: true).pop();
    setState(() {});
  }
}

class MyWidgetFactory extends WidgetFactory with WebViewFactory {
  // optional: override getter to configure how WebViews are built
  bool get webViewMediaPlaybackAlwaysAllow => true;

  String? get webViewUserAgent => 'iAM Logger';

  bool get webViewDebuggingEnabled => false;
}
