class DeviceStatus {
  DeviceStatus(this.name, this.lastReceiveDate, this.current);

  final String name;
  final String lastReceiveDate;
  final String current;
}
