class LogData {
  LogData(this.data, this.lastIndex);

  final String data;
  final int lastIndex;
}
